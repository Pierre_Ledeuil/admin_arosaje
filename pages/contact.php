<?php include("header.php") ?>

<p>
    Pour toutes demandes (demades spécifiques à l'équipe, SAV,
    réclamation, ...) merci de nous contacter par mail ci-dessous.
</p>

<style>
    body {
    
    background: url('../images/foret.jpg') ;
    background-repeat: no-repeat;
    background-position: center;
    background-attachment: fixed;
    background-size: cover;
    background-repeat: no-repeat;
    background-blend-mode: lighten;
    height: 100vh;
    overflow-x: hidden;
}

    fieldset {
        padding: 10px;
        margin-bottom: 20px;
    }
    input[type="text"], input[type="email"], input[type="tel"], textarea
{
  width: 90% !important;
}
    form>fieldset {
        
  background-color: rgba(255, 255, 255, 0.9);
    }

    legend {
        font-weight: bold;
        color: #333;
    }

    label {
        display: block;
        margin-bottom: 5px;
    }

    input[type="text"],
    input[type="email"],
    input[type="tel"],
    textarea {
        width: 100%;
        padding: 8px;
        border: 1px solid #ccc;
        border-radius: 5px;
        margin-bottom: 10px;
    }

    input[type="submit"] {
        padding: 10px 20px;
        background-color: #007bff;
        color: #fff;
        border: none;
        border-radius: 5px;
        cursor: pointer;
    }

    input[type="submit"]:hover {
        background-color: #0056b3;
    }

    .grid {
        display: grid;
        grid-template-columns: repeat(3, 1fr);
        gap: 10px;
    }
    button {
        background-color: transparent;
    }
input[type="submit"]
{
  padding: 10px 20px;

  background-color: black;
}
div {
    margin-top: 4%;
}
#forML {
    color: white !important;
}
#forML a:visited
{
  color: white;
}
</style>
<div>
    <div>
        <form>
            <fieldset>
                <fieldset>
                    <legend> Vos Coordonnées </legend>
                    <!-- Mr, Mme, Mlle -->
                    <div class="grid">
                        <div>
                            <label for="Mr"> Monsieur </label>
                            <input type="radio" name="choix" id="Mr" value="1" checked="checked" tabindex="1"><br>
                        </div>

                        <div>
                            <label for="Mme"> Madame </label>
                            <input type="radio" name="choix" id="Mme" value="2" tabindex="2"> <br>
                        </div>

                        <div>
                            <label for="Mlle"> Mademoiselle </label>
                            <input type="radio" name="choix" id="Mlle" value="3" tabindex="3"> <br>
                        </div>
                    </div>

                    <!-- Nom, Prénom, Age, Couleur -->
                    <label for="nom"> Saisir votre nom : </label>
                    <input type="text" name="leNom" id="nom" required />
                    <br>

                    <label for="prenom"> Sairsir votre prénom : </label>
                    <input type="text" name="lePrenom" id="prenom" required /> <br>

                    <label for="naissance"> Votre date de naissance :
                    </label>
                    <input type="date" name="laDate" min="1900-01-01" id="naissance" required /> <br>
                </fieldset>

                <fieldset>
                    <!-- Mail, Objet msg, Msg-->
                    <legend> Votre Message </legend>
                    <label for="email"> Votre adresse mail : </label>
                    <input type="email" name="email" id="email" required />
                    <br>

                    <label for="number"> Votre numéro de téléphone :
                    </label>
                    <input type="tel" name="phone" id="number" /> <br>

                    <label for="objetmsg"> Objet du message : </label>
                    <input type="text" name="leObjet" id="objetmsg" required /> <br>

                    Saisir le message : <textarea name="taRe7" id="msg" cols="80" rows="10" required> </textarea> <br>
                </fieldset>

                <button> <input type="submit" value="Valider" id="submit"> </button>
            </fieldset>
        </form>
    </div>
    <div>
        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2887.2605670533712!2d3.8359923157227165!3d43.64274686112414!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x12b6aeb0554aff77%3A0x883bcb39fbb35b3b!2sEPSI%20Montpellier!5e0!3m2!1sfr!2sfr!4v1637610622218!5m2!1sfr!2sfr" width="674px" height="611px" style="border:0;" allowfullscreen="" loading="lazy"></iframe>
    </div>
</div>
<div>
    <p id="forML">
        <a href="ML.php">Mentions légales</a>
    </p>
</div>
</div>
<?php include("footer.php") ?>