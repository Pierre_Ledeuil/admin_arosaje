<?php include("header.php");

if ($_SERVER["REQUEST_METHOD"] == "POST") {
    $username = $_POST['username']; //requete base username admin
    $password = $_POST['password']; //requete base password admin

    // à adapter avec infos BdD
    $servername = "localhost";
    $username_db = "votre_nom_utilisateur";
    $password_db = "votre_mot_de_passe";
    $dbname = "arosaje";

    $conn = new mysqli($servername, $username_db, $password_db, $dbname);
    if ($conn->connect_error) {
        die("Connexion échouée: " . $conn->connect_error);
    }

    $sql = "SELECT * FROM customer WHERE username = '$username'";
    $result = $conn->query($sql);

    if ($result->num_rows == 1) {
        $row = $result->fetch_assoc();
        if (password_verify($password, $row['password'])) {
            $_SESSION['user_id'] = $row['id'];
            header("Location: ../index.php");
            exit;
        } else {
            $error = "Nom d'utilisateur ou mot de passe incorrect.";
        }
    } else {
        $error = "Nom d'utilisateur ou mot de passe incorrect.";
    }

    $conn->close();
}

/*
if ($_POST['username'] === $username && $_POST['password'] === $password) {
    $_SESSION['user_id'] = $username;
    header("Location: ../index.php");
    exit;
} else {
    $error = "Nom d'utilisateur ou mot de passe incorrect.";
}
*/

?>

<body>

    <section id="connexion">
        <div class="login-container">
            <h2>Connexion</h2>
            <form action="#" method="post">
                <div class="form-group">
                    <label for="username">Nom d'utilisateur :</label>
                    <input type="text" id="username" name="username" required>
                </div>
                <div class="form-group">
                    <label for="password">Mot de passe :</label>
                    <input type="password" id="password" name="password" required>
                </div>
                <div class="form-group">
                    <button type="submit">Se Connecter</button>
                </div>
            </form>
        </div>
    </section>

    <section id="creation">
        <div class="login-container">
            <h2>Création compte</h2>
            <form action="#" method="post">
                <div class="form-group">
                    <label for="username">Nom d'utilisateur :</label>
                    <input type="text" id="username" name="username" required>
                </div>
                <div class="form-group">
                    <label for="password">Mot de passe :</label>
                    <input type="password" id="password" name="password" required>
                </div>
                <div class="form-group">
                    <button type="submit">Se Connecter</button>
                </div>
            </form>
        </div>
    </section>

</body>

</html>

<?php include("footer.php") ?>