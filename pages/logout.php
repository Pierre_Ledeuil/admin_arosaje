<?php
include("header.php");

session_destroy();

if(!empty($_SESSION)) {
	header("Location: logout.php");
}
?>

<!DOCTYPE html>

<body>
    <h2>Vous avez été déconnecté avec succès.<br>
    <a href="../index.php">Retour à l'accueil</a></h2>
</body>

</html>