<?php include("pages/header.php") ?>

<section id="accueil">
	<div class="centerAccueil">
		<div>
			<h2>Notre Histoire</h2>
		</div>
	</div>
</section>
<section id="about">
	<div>
		<div class="aboutText">
			<h2>Qui sommes nous ?</h2>
			<p>
				Au sein de notre groupe de passionnés de jardinage, l'idée de créer un site dédié à la garde de plantes a émergé naturellement. Animés par notre amour commun pour la verdure, nous avons constaté les défis auxquels nous étions confrontés chaque fois que nous devions nous absenter, laissant nos plantes sans surveillance.
				<br><br>
				Notre équipe, composée de personnes aux compétences diverses, s'est rapidement formée. Des développeurs web compétents, des experts en horticulture chevronnés et des esprits créatifs ont uni leurs forces pour transformer notre vision en réalité. Notre objectif était clair : concevoir une plateforme en ligne qui simplifierait la recherche de gardiens qualifiés pour nos plantes bien-aimées.
				<br><br>
				Les mois de collaboration intense et de développement ont donné naissance à une plateforme intuitive. Chacun d'entre nous a contribué à la création d'un espace où les utilisateurs peuvent détailler les besoins spécifiques de leurs plantes, et où des gardiens compétents peuvent être trouvés en un clic.
				<br><br>
				Ainsi, notre site est né, une concrétisation de notre passion commune. Il va au-delà d'une simple plateforme en ligne, reflétant notre engagement envers la communauté des amoureux de plantes. C'est notre histoire, une aventure collective visant à simplifier la vie des passionnés de verdure, à créer des liens entre des individus partageant une passion commune, et à offrir une tranquillité d'esprit lorsque nos plantes restent entre de bonnes mains en notre absence. Cette histoire est un témoignage de notre dévouement envers la nature, de notre désir de partager notre amour des plantes avec le monde.
			</p>
				
			</div>
		<div class="aboutImg">
			<img src="images/plante.jpg" alt="Plante">
		</div>
	</div>
	<div>
		<div class="aboutImg">
			<img src="images/engagement.jpg" alt="Engagement">
		</div>
		<div class="aboutText">
			<h2>Notre engagement</h2>
			<p>
				Notre engagement envers la communauté des amoureux de plantes est le cœur battant de notre site. En créant cette plateforme, nous nous sommes engagés à construire bien plus qu'un simple service en ligne – nous avons aspiré à créer une communauté florissante, unissant des individus partageant la même passion pour la nature.
				<br><br>
				En tant que fervents défenseurs de la biodiversité domestique, nous nous engageons à fournir une plateforme qui va au-delà des simples transactions. Nous avons créé un espace où chaque plante a une histoire, où chaque gardien partage un amour authentique pour la verdure. Notre engagement se manifeste dans la rigueur de nos critères de sélection pour les gardiens, assurant ainsi que seules des mains expertes prennent soin des plantes confiées.
				<br><br>
				La transparence est un autre pilier fondamental de notre engagement. Nous avons mis en place des outils pour faciliter la communication ouverte entre les propriétaires de plantes et les gardiens, établissant ainsi une confiance mutuelle. Nous croyons que la transparence est essentielle pour construire des relations solides au sein de notre communauté.
				<br><br>
				En tant qu'acteurs de la préservation de la biodiversité domestique, nous nous engageons également à sensibiliser notre communauté aux pratiques éco-responsables. Des conseils sur l'arrosage conscient aux astuces pour favoriser une croissance saine, notre site se veut être une ressource éducative pour tous les membres de notre communauté.
			</p>
				
		</div>
	</div>
</section>
<section id="accueil">
	<div class="centerAccueil">
		<div>
			<a href="pages/connexion.php"><h2 style="text-decoration: underline;">Rejoignez-nous</h2></a>
		</div>
	</div>
</section>

<?php include("pages/footer.php") ?>